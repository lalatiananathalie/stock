$(".sidebar-dropdown > a").click(function() {
    $(".sidebar-submenu").slideUp(200);
    if (
      $(this)
        .parent()
        .hasClass("active")
    ) {
      $(".sidebar-dropdown").removeClass("active");
      $(this)
        .parent()
        .removeClass("active");
    } else {
      $(".sidebar-dropdown").removeClass("active");
      $(this)
        .next(".sidebar-submenu")
        .slideDown(200);
      $(this)
        .parent()
        .addClass("active");
    }
  });
  
  $("#close-sidebar,#desktop").click(function() {
    $(".page-wrapper").removeClass("toggled");
  });
  $("#show-sidebar").click(function() {
    $(".page-wrapper").addClass("toggled");
  });
  
  $('#mobile').on('click', function(){
      var imageUrl = "../../asset/img/phone.png";
		  $('div.vue').css({'max-width':350 , 'height':706, "background-image": "url(" + imageUrl + ")", "background-size":"cover"});
      $('div.vue iframe').css({'height':'100%','padding-top' : 82, 'padding-left': 24, 'padding-bottom': 86, 'padding-right': 18});
      $(".page-wrapper").addClass("toggled");
	});
  $('#tablet').on('click', function(){
    var imageUrlTab = "../../asset/img/tablette.png";
    $('div.vue').css({'max-width':1025 , 'height':671, "background-image": "url(" + imageUrlTab + ")", "background-size":"cover"});
    $(".page-wrapper").addClass("toggled");
    $('div.vue iframe').css({'height':'100%','padding-top' : 42, 'padding-left': 117, 'padding-bottom': 44, 'padding-right': 125});
  });
  $('#desktop').on('click', function(){
    $('div.vue').css({'max-width':'100%' , 'height':'100vh', "background-image": "none"});
    $('div.vue iframe').css('padding' , 0);
  });