<?php

namespace App\Form;

use App\Entity\Commande;
use App\Entity\Client;
use App\Entity\Produit;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommandeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('date', DateType::class, [
                'label' => false,
                'widget' => 'single_text'
            ])
            ->add('numero', TextType::class, [
                'label' => false
            ])
            ->add('quantiteCommande', IntegerType::class, [
                'label' => false
            ])
            ->add('client', EntityType::class, [
                'class' => Client::class,
                'placeholder' => '',
                'label' => false
            ])
            ->add('produit', EntityType::class, [
                'class' => Produit::class,
                'multiple' => true,
                'expanded' => true,
                'placeholder' => '',
                'label' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Commande::class,
        ]);
    }
}
