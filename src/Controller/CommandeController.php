<?php

namespace App\Controller;

use App\Entity\Commande;
use App\Entity\Produit;
use App\Form\CommandeType;
use App\Repository\CommandeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/commande")
 */
class CommandeController extends AbstractController
{
    /**
     * @Route("/", name="commande_index", methods={"GET"})
     */
    public function index(CommandeRepository $commandeRepository): Response
    { 
        // dd($commandeRepository->findAll());
        return $this->render('commande/listCommande.html.twig', [
            'commandes' => $commandeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/detail/{id}", name="commande_detail", methods={"GET"})
     */
    public function detail($id, CommandeRepository $commandeRepository): Response
    { 
        return $this->render('commande/detailCommande.html.twig', [
            'commande' => $commandeRepository->find($id),
        ]);
    }

    /**
     * @Route("/new", name="commande_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $commande = new Commande();
        $form = $this->createForm(CommandeType::class, $commande);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            foreach ($form->get('produit')->getData() as $produit) {
                $produit->setQuantiteStock($produit->getQuantiteStock() - (int)$form->get('quantiteCommande')->getData());
                $entityManager->persist($produit);
                $entityManager->flush();
            }
            $entityManager->persist($commande);
            $entityManager->flush();
            $this->addFlash('info', 'Commande ajouté avec succès');

            return $this->redirectToRoute('commande_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('commande/newCommande.html.twig', [
            'commande' => $commande,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="commande_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Commande $commande): Response
    {
        $form = $this->createForm(CommandeType::class, $commande);
        $form->handleRequest($request);


            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                foreach ($commande->getProduit() as $produit) {
                    $produit->setQuantiteStock($produit->getQuantiteStock() + abs($commande->getQuantiteCommande() ));
                    $entityManager->persist($produit);
                    $entityManager->flush();
                }
                $entityManager->flush();
                return $this->redirectToRoute('commande_index', [], Response::HTTP_SEE_OTHER);
            }

        return $this->render('commande/editCommande.html.twig', [
            'commande' => $commande,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="commande_delete", methods={"POST"})
     */
    public function delete(Request $request, Commande $commande): Response
    {
        if ($this->isCsrfTokenValid('delete'.$commande->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            foreach ($commande->getProduit() as $produit) {
                $produit->setQuantiteStock($produit->getQuantiteStock() + $commande->getQuantiteCommande());
                $entityManager->persist($produit);
                $entityManager->flush();
            }
            $entityManager->remove($commande);
            $entityManager->flush();
        }

        return $this->redirectToRoute('commande_index', [], Response::HTTP_SEE_OTHER);
    }
}
