<?php

namespace App\Controller;

use App\Entity\Fournisseur;
use App\Entity\Marque;
use App\Entity\Produit;
use App\Form\ProduitType;
use App\Repository\MarqueRepository;
use App\Repository\ProduitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/produit")
 */
class ProduitController extends AbstractController
{
    /**
     * @Route("/", name="produit_index", methods={"GET"})
     */
    public function index(ProduitRepository $produitRepository): Response
    {
        return $this->render('produit/listProduit.html.twig', [
            'produits' => $produitRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="produit_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $produit = new Produit();
        $form = $this->createForm(ProduitType::class, $produit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($produit);
            $entityManager->flush();
            $this->addFlash('info', 'Produit ajouté avec succès');
            $session = $request->getSession();
            $session->set('produit', $produit->getId());
            return $this->redirectToRoute('marque_fournisseur', ['id' => $produit->getFournisseur()->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('produit/newProduit.html.twig', [
            'produit' => $produit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new/marque/{id}", name="marque_fournisseur", methods={"GET","POST"})
     */
    public function marqueFournisseur(Request $request, Fournisseur $fournisseur, EntityManagerInterface $em): Response
    {
        $marques = $em->getRepository(Marque::class)->findBy(['fournisseur' => $fournisseur]);
        if($request->isMethod('post')) {
            $produit = $em->getRepository(Produit::class)->find($request->getSession()->get('produit'));
            $produit->setMarque($em->getRepository(Marque::class)->find((int)$request->request->get('marque')));
            $em->persist($produit);
            $em->flush();
            $this->addFlash('info', 'Produit lié au marque avec succès');
            return $this->redirectToRoute('produit_index');
        }
        return $this->render('marque/listMarqueFournisseur.html.twig', ['marques' => $marques, 'id' => $fournisseur->getId()]);
    }

    /**
     * @Route("/{id}/edit", name="produit_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Produit $produit): Response
    {
        $form = $this->createForm(ProduitType::class, $produit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('produit_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('produit/editProduit.html.twig', [
            'produit' => $produit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="produit_delete", methods={"POST"})
     */
    public function delete(Request $request, Produit $produit): Response
    {
        if ($this->isCsrfTokenValid('delete'.$produit->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($produit);
            $entityManager->flush();
        }

        return $this->redirectToRoute('produit_index', [], Response::HTTP_SEE_OTHER);
    }
}
