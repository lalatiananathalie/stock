<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Produit;
// use App\Tests\Http\RequestBuilder;
// use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProduitTest extends ApiTestCase
{



    public function testPOST():void
       {
        $titre = 'Some titre'.rand(0, 999);
        $description = 'Some description'.rand(0, 999);
        $response = static::createClient()->request('POST', '/api/produits', ['json' => [
             // "body" => "Test",
              "titre" => "Titre",
              "description" => "description",
              "quantiteStock" => 10,
              "prix" => "5000",
              "types" => "A test type",
              "fournisseur"=> "/api/fournisseurs/3"
            ]
        ]);

       $this->assertResponseStatusCodeSame(201);

      }

    public function testGET():void
    {
        $titre = 'Some titre'.rand(0, 999);
        $description = 'Some description'.rand(0, 999);
        $response = static::createClient()->request('GET', '/api/produits/47');

        $this->assertResponseStatusCodeSame(200);

    }

    public function testPUT():void
    {
        $titre = 'Some titre'.rand(0, 999);
        $description = 'Some description'.rand(0, 999);
        $response = static::createClient()->request('PUT', '/api/produits/47', ['json' => [
            // "body" => "Test",
            "titre" => "Titre",
            "description" => "description",
            "quantiteStock" => 10,
            "prix" => "5000",
            "types" => "A test type",
            "fournisseur"=> "/api/fournisseurs/3"
        ],
            'headers' => [
                'accept' => 'application/ld+json',
                'Content-Type' => 'application/ld+json',
            ]
        ]);

        $this->assertResponseStatusCodeSame(200);

    }

    public function testPATCH():void
    {
        $titre = 'Some titre'.rand(0, 999);
        $description = 'Some description'.rand(0, 999);
        $response = static::createClient()->request('PATCH', '/api/produits/47', ['json' => [
            // "body" => "Test",
            "titre" => "Titre",
            "description" => "description",
            "quantiteStock" => 10,
            "prix" => "5000",
            "types" => "A test type",
            "fournisseur"=> "/api/fournisseurs/3"
        ],
            'headers' => [
                'accept' => 'application/ld+json',
                'Content-Type' => 'application/merge-patch+json',
            ]
        ]);

        $this->assertResponseStatusCodeSame(200);

    }


}
